/*Autor : Prashinka Sahu
 * Date : 26th Feb 2018
 *  Purpose : This class is used to contains tests we are covering in Orbitz Automation flow. 
 * 
 */

package orbitz.test;

import org.testng.annotations.Test;

import orbitz.services.Services;

public class Flight_Booking_Test extends Browser_Activities {
	Services serv = new Services();
	

	/*@Test 
	public void oneWay_Booking() throws Exception {
		driver.get("https://www.orbitz.com/"); // Opening orbitz Url
		System.out.println("Scenario:Oneway booking Test");
		System.out.println("URL opened for : " + driver.getTitle());
		serv.open_Flight_Booking_Page(driver);
		serv.select_Journey_Type("oneway", driver);
		serv.select_Cities(driver, "Jabalpur, India", "Indore, India"); // Selecting cities from and to
		serv.select_Date(driver, "departing", "12-JUN-2018"); // Selecting departure date
		serv.take_Screenshot(driver, "Before_Search"); // Taking screenshot before search
		serv.click_Search_Button(driver);
		serv.verify_Results(driver, "Jabalpur, India", "Indore, India");
		serv.take_Screenshot(driver, "After Search"); // Taking screenshot after search
	
	}*/
	
	@Test
	public void roundTrip_Booking() throws Exception {
		driver.get("https://www.orbitz.com/"); // Opening orbitz Url
		System.out.println("Scenario : Roundtrip booking Test");
		System.out.println("URL opened for : " + driver.getTitle());
		serv.open_Flight_Booking_Page(driver);
		serv.select_Journey_Type("roundtrip", driver);
		serv.select_Cities(driver, "Mumbai, India", "Jabalpur, India"); // Selecting cities from and to
		serv.select_Date(driver, "departing", "12-JUN-2018"); // Selecting departure date
		serv.select_Date(driver, "returning", "19-JUN-2018"); // Selecting returning date
		serv.take_Screenshot(driver, "Before_Search"); // Taking screenshot before search
		serv.click_Search_Button(driver);
		serv.verify_Results(driver, "Mumbai, India", "Jabalpur, India");
		serv.take_Screenshot(driver, "After Search"); // Taking screenshot after search
	}

}
