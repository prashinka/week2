/*Autor : Prashinka Sahu
 * Date : 26th Feb 2018
 *  Purpose : This class is used to create and initialize browser. 
 * 
 */



package orbitz.test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

public class Browser_Activities {
	WebDriver driver;

	@BeforeClass
	@Parameters({ "browser_Name" })
	public void Open_Browser(String browser_Name) {
		if (browser_Name.equalsIgnoreCase("Chrome")) {
			System.setProperty("webdriver.chrome.driver", "drivers\\chromedriver.exe");
			driver = new ChromeDriver();
			System.out.println("Opening Chrome");

		} else if (browser_Name.equalsIgnoreCase("Firefox")) {
			driver = new FirefoxDriver();
			System.out.println("Opening firefox");
		} else {
			System.out.println("Opening Chrome as default browser");
			System.setProperty("webdriver.chrome.driver", "drivers\\chromedriver.exe");
			driver = new ChromeDriver();
		}
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	@AfterClass
	public void close_Browser() {
		driver.close();
		System.out.println("Browser Closed");

	}
}