/*Autor : Prashinka Sahu
 * Date : 26th Feb 2018
 * Purpose : Lister class implement in Orbitz Application automation flow.
 */

package orbitz.test;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import orbitz.services.Services;

public class TestListener implements ITestListener {

	Services serve = new Services();

	@Override
	public void onTestSuccess(ITestResult result) {

	System.out.println("========================== Test Completed Succesfully============================");
	
	}

	@Override
	public void onTestFailure(ITestResult result) {
		System.out.println("================================= Test Failed===================================");
	}

	@Override
	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub

	}

}
