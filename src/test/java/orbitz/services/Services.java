/* Author : Prashinka Sahu
 * Date : 26 Feb 2018
 * Purpose : This class contains all the required services we can perform on 
 * Orbitz Application.
 */

package orbitz.services;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import orbitz.pages.Flights_Results;
import orbitz.pages.Flights_Search;
import orbitz.pages.Home;

public class Services {

	Flights_Search fs = new Flights_Search();
	Home home_Obj = new Home();
	Flights_Results res = new Flights_Results();

	public void take_Screenshot(WebDriver driver, String name) throws Exception {
		try {
			TakesScreenshot tks = (TakesScreenshot) driver;
			File scrFile = tks.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File("Screenshots\\" + name + "_" + System.currentTimeMillis() + ".png"));
		} catch (Exception e) {
			System.out.println("Error in taking screenshot :" + e.getMessage());
		}
	}

	public void open_Flight_Booking_Page(WebDriver driver) {
		home_Obj.create_Flights_Only(driver).click();
		System.out.println("Opening Flight Booking Page");

	}

	public void select_Journey_Type(String journey_Type, WebDriver driver) {

		if (journey_Type.equalsIgnoreCase("OneWay"))
			fs.create_OneWay_Trip(driver).click();

		else
			fs.create_RoundTrip(driver).click();

		System.out.println("Trip Selected as : " + journey_Type);
	}

	public void select_Cities(WebDriver driver, String from, String to) {

		// Selecting Source City
		WebElement source_City = fs.create_Flying_From(driver);
		source_City.clear();
		source_City.sendKeys(from);
		source_City.sendKeys(Keys.TAB);
		// Selecting Destination City
		WebElement to_City = fs.create_Flying_To(driver);
		to_City.clear();
		to_City.sendKeys(to);
		to_City.sendKeys(Keys.TAB);
		System.out.println("Searching from " + from + " to " + to);

	}

	public void select_Date(WebDriver driver, String journey_Type, String travel_Date) throws Exception {
		String day = travel_Date.substring(0, 2);
		String mon = travel_Date.substring(3, 6);
		String year = travel_Date.substring(7, 11);
		WebElement date_Box = new WebDriverWait(driver, 30)
				.until(ExpectedConditions.presenceOfElementLocated(By.id("flight-" + journey_Type)));
		date_Box.click();
		Thread.sleep(1000);// Using for firefox
		date_Box.click();// due application issue on firefox
		boolean flag = true;
		WebElement date_Division = driver.findElement(By.xpath(".//*[@id='flight-" + journey_Type + "-wrapper']/div"));
		do {
			if (date_Division.isDisplayed()) {
				List<WebElement> calender_Month = date_Division.findElements((By.cssSelector(".datepicker-cal-month")));
				calender_Loop: for (int i = 0; i < calender_Month.size(); i++) {
					String month_Header = calender_Month.get(i).findElement(By.className("datepicker-cal-month-header"))
							.getText();
					if (month_Header.equalsIgnoreCase(mon + " " + year)) {
						calender_Month.get(i).findElement(By.xpath("//button[text()='" + day + "']")).click();
						System.out.println("Date of " + journey_Type + " selected as :" + day + "/" + mon + "/" + year);
						flag = false;
						break calender_Loop;

					} // ending month if
					else {
						if (i == 1)
							driver.findElement(
									By.xpath(".//*[@id='flight-" + journey_Type + "-wrapper']/div/div/button[2]"))
									.click();

					}
				}
			}
		} while (flag);
	}

	public void click_Search_Button(WebDriver driver) {
		fs.create_Search_Button(driver).click();
		System.out.println("Clicked Search Button");
	}

	public void verify_Results(WebDriver driver, String source, String destination) {

		// validating result title and count
		if (res.create_Result_Title(driver).getText()
				.contains(destination.substring(0, destination.indexOf(',') - 1))) {
			System.out.println("Under if");
			List<WebElement> result_List = res.create_Result_List(driver);
			System.out.println("Total result appearing for " + source + " to " + destination + " is "
					+ result_List.size() + "\r\n");
			String destination_Code = res.create_Destination_CityCode(driver).getAttribute("value");
			String source_Code = res.create_Source_CityCode(driver).getAttribute("value");
			destination_Code = destination_Code.substring(destination_Code.indexOf('(') + 1,
					destination_Code.indexOf(')') - 1);
			source_Code = source_Code.substring(source_Code.indexOf('(') + 1, source_Code.indexOf(')') - 1);

			
			List<WebElement> airline_Names = driver
					.findElements(By.xpath("//span[contains(@data-test-id,'airline-name')]"));
			// validating city code in each result
			for (int i = 0; i < result_List.size(); i++) {
				if ((result_List.get(i).getText().contains(source_Code))
						&& (result_List.get(i).getText().contains(destination_Code))) {
					System.out.println("Result " + (i + 1) + " : " + (airline_Names.get(i).getText()) + "\r\n"
							+ result_List.get(i).getText() + "\r\n");

				} else {
					Assert.fail("Result are not accurate!!");
				}

			}

		} else {
			Assert.fail("Result are not accurate!!");
		}
	}

}
